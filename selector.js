$("#id_type_data").change(function() {
  var value = $("#id_bool_value").val() || $("#id_int_value").val() || $("#id_float_value").val() || $("#id_text_value").val();
  //alert(value);
  if ($(this).val() == 'Bol') {
    value != 0 ? true : false;
    $("#id_int_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_float_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_text_value").val(null).closest("div").parent().closest("div").hide();

    $("#id_bool_value").val(value).closest("div").parent().closest("div").show();
  }

  if ($(this).val() == 'Int') {
    $("#id_bool_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_float_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_text_value").val(null).closest("div").parent().closest("div").hide();

    $("#id_int_value").val(value).closest("div").parent().closest("div").show();
  }

  if ($(this).val() == 'Flo') {
    $("#id_bool_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_int_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_text_value").val(null).closest("div").parent().closest("div").hide();

    $("#id_float_value").val(value).closest("div").parent().closest("div").show();
  }

  if ($(this).val() == 'Txt') {
    $("#id_bool_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_float_value").val(null).closest("div").parent().closest("div").hide();
    $("#id_int_value").val(null).closest("div").parent().closest("div").hide();

    $("#id_text_value").val(value).closest("div").parent().closest("div").show();
  }
});
