import sys
from django.contrib.admin.models import LogEntry
from django.core import serializers
from django.contrib.sessions.base_session import AbstractBaseSession
import datetime
from .models import ActionLog


class MyMiddleware(object):

    def process_response( self, request, response):

        # logs = LogEntry.objects.latest('id')
        if request.user.id:
            log = ActionLog(
                user_id=request.user.id,
                action=request,
                params=request.POST,
                time_in=datetime.datetime.now(),
            )
            log.save()
        return response


class TestMiddleware(object):
    sys.stdout = open('logtime.log', "a")

    def process_request(self, request):
        self.time = datetime.datetime.now()
        print 'process_request', self.time
        return None

    def process_response(self, request, response):
        self.dif_time =  datetime.datetime.now() - self.time
        print 'process_response_diff', self.dif_time
        return response

