# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
# from django.contrib.contenttypes.models import ContentType
# from django.contrib.contenttypes.fields import GenericForeignKey
from django.conf import settings
from django.utils import timezone
from django.contrib.admin.models import LogEntry
import datetime
import csv
import os
# Create your models here.


def validate_int(value):
    if not isinstance(value, int):
        raise ValidationError(u'%s не целое число' % value)


def validate_float(value):
    if not isinstance(value, float):
        raise ValidationError(u'%s не вещественное число' % value)


def validate_bool(value):
    if not isinstance(value, bool):
        raise ValidationError('%s не вещественное значение' % value)


class ContentThree(models.Model):
    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'

    TYPE_CHOICE = (
        ('Bol', 'Логический'),
        ('Int', 'Целочисленный'),
        ('Flo', 'Вещественный'),
        ('Txt', 'Текст'),
    )

    title = models.CharField(u'Название записи', max_length=100)
    type_data = models.CharField(u'Тип записи', max_length=3, choices=TYPE_CHOICE)

    text_value = models.CharField(u'Текст', max_length=100, blank=True, null=True)
    int_value = models.IntegerField(u'Целое число', null=True, blank=True, validators=[validate_int])
    float_value = models.FloatField(u'Вещественное число', null=True, blank=True, validators=[validate_float])
    bool_value = models.BooleanField(u'Верно или нет', validators=[validate_bool])

    def __unicode__(self):
        return self.title

    def smt_value(self):
        type_d = self.type_data
        if type_d == 'Bol':
            return self.bool_value
        elif type_d == 'Int':
            return self.int_value
        elif type_d == 'Flo':
            return self.float_value
        elif type_d == 'Txt':
            return self.text_value
        else:
            return 'Empty'
    # smt_value.boolean = True
    smt_value.short_description = 'Значение'

    def save(self, *args, **kwargs):
        super(ContentThree, self).save(*args, **kwargs)
        logs = LogEntry.objects.latest('id')
        with open(settings.PROJECT_PATH + '/data.csv', 'a') as csvfile:
            fieldnames = [
                # 'logs',
                'title',
                'type_data',
                'text',
                'int',
                'float',
                'bool',
                'time',
            ]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            if not os.path.exists(settings.PROJECT_PATH + '/data.csv'):
                writer.writeheader()

            writer.writerow({
                # 'logs': logs,
                'title': str(self.title),
                'type_data': str(self.type_data),
                'text': str(self.text_value),
                'int': str(self.int_value),
                'float': str(self.float_value),
                'bool': str(self.bool_value),
                'time': logs.action_time,
            })

        # super(ContentThree, self).save(*args, **kwargs)



# class Music(models.Model):
#     music_name = models.CharField(max_length=30)
#     duration = models.IntegerField()
#
#
# class Picture(models.Model):
#     picture_name = models.CharField(max_length=100)
#     author = models.CharField(max_length=100)
#
#
# class Video(models.Model):
#     name_video = models.CharField(max_length=50)
#     content = models.IntegerField()
#     duration = models.IntegerField()
#
#
# class TaggedItem(models.Model):
#     tag = models.SlugField()
#     content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
#     object_id = models.PositiveIntegerField()
#     content_object = GenericForeignKey('content_type', 'object_id')
#
#     def __str__(self):              # __unicode__ on Python 2
#         return self.tag
#
#


class ActionLog(models.Model):
    user_id = models.PositiveIntegerField()
    action = models.CharField(max_length=100)
    params = models.CharField(max_length=50)
    time_in = models.DateTimeField()


    def admin_inform(self):
        self.warning = (self.time_in >= timezone.now() - datetime.timedelta(days=365)*5)

    def save(self, *args, **kwargs):
        # if ActionLog.filter(user_id=self.user_id).
        super(ActionLog, self).save(*args, **kwargs)



    # def bool_format_out(self):
    #     return self.bool_value
    # bool_format_out.boolean = True


    # def __str__(self):
    #     return self.title