
$(document).ready(function() {
    $("#id_type_data").change(function() {
        var value = $("#id_int_value").val() || $("#id_float_value").val() || $("#id_text_value").val() || $("#id_bool_value").val()
//        alert(value);

        $("fieldset.module.aligned:last").addClass("hiddenAll");
        $(".hiddenAll").removeClass("Bol Int Flo Txt");

        if ($(this).val() == 'Bol') {
            $(".hiddenAll").addClass("Bol");
        }

        if ($(this).val() == 'Int') {
            $(".hiddenAll").addClass("Int");
        }

        if ($(this).val() == 'Flo') {
            $(".hiddenAll").addClass("Flo");
        }

        if ($(this).val() == 'Txt') {
            $(".hiddenAll").addClass("Txt");
        }
    });
});