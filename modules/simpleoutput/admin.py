# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import ContentThree


class FormAdmin(admin.ModelAdmin):
    empty_value_display = 'Не задано'
    fieldsets = [
        ('Титул', {'fields': ['title']}),
        ('Тип', {'fields': ['type_data']}),
        ('Значение', {'fields': [
            'int_value',
            'float_value',
            'bool_value',
            'text_value',
        ]})
    ]

    # def save_model(self, request, obj, form, change):
    #     pass

    class Media:
        js = ['js/jquery-3.2.1.min.js',
              'js/selector.js',
              #'js/test.js',
              ]
        css = {
            'all': ('css/select.css',),
        }
    # fields = ['title', 'type_data', 'int_value', 'float_value', 'bool_value', 'text_value']
    list_display = ('title', 'type_data', 'smt_value')


admin.site.register(ContentThree, FormAdmin)

#     #в админке сделать
#     type_v = type(value_three)
#     if type_v == int:
#         #отображение целых чисел int()
#     elif type_v == bool:
#         if value_three == 0:
#             #отображение ... (в чекбоксе крестик)
#         else:
#             #отображение ... (в чекбоксе галочка)
#     elif type_v == float:
#         # отображение числа
#     else:
#         # иначе текст
#     # list_display = ('title_three', 'type_three', 'value_three_method')