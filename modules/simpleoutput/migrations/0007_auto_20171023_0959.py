# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-23 06:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('simpleoutput', '0006_auto_20171020_1442'),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
            ],
        ),
        migrations.AlterModelOptions(
            name='contentthree',
            options={'verbose_name': '\u0417\u0430\u043f\u0438\u0441\u044c', 'verbose_name_plural': '\u0417\u0430\u043f\u0438\u0441\u0438'},
        ),
        migrations.AlterField(
            model_name='contentthree',
            name='text_value',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442'),
        ),
        migrations.AlterField(
            model_name='contentthree',
            name='type_data',
            field=models.CharField(choices=[('Bol', '\u041b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0438\u0439'), ('Int', '\u0426\u0435\u043b\u043e\u0447\u0438\u0441\u043b\u0435\u043d\u043d\u044b\u0439'), ('Flo', '\u0412\u0435\u0449\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439'), ('Txt', '\u0422\u0435\u043a\u0441\u0442')], max_length=3, verbose_name='\u0422\u0438\u043f \u0437\u0430\u043f\u0438\u0441\u0438'),
        ),
        migrations.CreateModel(
            name='MyPerson',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('simpleoutput.person',),
        ),
    ]
